import tkinter as tk

def generate_card():
    name = name_entry.get()
    age = age_entry.get()
    email = email_entry.get()
    do_work = work_var.get()
    
    if do_work:
        company = company_entry.get()
        package = package_entry.get()
        result_text = f"Name: {name}\nAge: {age}\nEmail: {email}\nWork: Yes\nCompany: {company}\nPackage: {package}\n\n"
    else:
        result_text = f"Name: {name}\nAge: {age}\nEmail: {email}\nWork: No\n\n"
    
    new_card = tk.Label(card_frame, text=result_text, bg="green", relief=tk.RIDGE, width=50, height=10)
    new_card.pack(side=tk.LEFT, padx=5)  # Pack new card to the left within the card_frame
    
   
    name_entry.delete(0, tk.END)
    age_entry.delete(0, tk.END)
    email_entry.delete(0, tk.END)
    work_var.set(False)
    company_entry.delete(0, tk.END)
    package_entry.delete(0, tk.END)
    generate_button.config(bg="SystemButtonFace")  


root = tk.Tk()
root.title("Card Generator")

name_label = tk.Label(root, text="Name:")
name_label.pack()

name_entry = tk.Entry(root)
name_entry.pack()

age_label = tk.Label(root, text="Age:")
age_label.pack()

age_entry = tk.Entry(root)
age_entry.pack()

email_label = tk.Label(root, text="Email:")
email_label.pack()

email_entry = tk.Entry(root)
email_entry.pack()

work_var = tk.BooleanVar()
work_checkbox = tk.Checkbutton(root, text="Do you work?", variable=work_var, onvalue=True, offvalue=False)
work_checkbox.pack()

company_label = tk.Label(root, text="Company:")
company_label.pack()

company_entry = tk.Entry(root)
company_entry.pack()

package_label = tk.Label(root, text="Package:")
package_label.pack()

package_entry = tk.Entry(root)
package_entry.pack()

generate_button = tk.Button(root, text="Generate Card", command=generate_card, bg="green", fg="black")  # Set initial background and foreground color
generate_button.pack()

#clear_button = tk.Button(root, text="Clear Form", command=lambda: result_label.config(text=""))
#clear_button.pack()


card_frame = tk.Frame(root)
card_frame.pack()

root.mainloop()