import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../models/note_model.dart';
import '../../services/database_helper.dart';
import '../../widgets/note_widget.dart';
import './note_screen.dart';

class UserScreen extends StatefulWidget {
  const UserScreen({Key? key}) : super(key: key);

  @override
  State<UserScreen> createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(100),
          child: AppBar(
            title: const Column(
              children: [
                SizedBox(
                  height: 30,
                ),
                Padding(
                  padding: EdgeInsets.all(40.0),
                  child: Text(
                    'Hello Recruiter,',
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  ),
                ),
                Stack(children: [Text("")]),
              ],
            ),
            centerTitle: true,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(50),
                bottomRight: Radius.circular(50),
              ),
            ),
            backgroundColor: const Color.fromARGB(255, 210, 102, 186),
          ),
        ),
        
        body: Padding(
          padding: const EdgeInsets.only(top: 70),
          child: FutureBuilder<List<Note>?>(
            future: DatabaseHelper.getAllNotes(),
            builder: (context, AsyncSnapshot<List<Note>?> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const CircularProgressIndicator();
              } else if (snapshot.hasError) {
                return Center(child: Text(snapshot.error.toString()));
              } else if (snapshot.hasData) {
                if (snapshot.data != null) {
                  return ListView.builder(
                    itemBuilder: (context, index) {
                      Note note = snapshot.data![index];
                      return Column(
                        children: [
                          NoteWidget(
                            note: note,
                            onTap: () async {
                              await Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => NoteScreen(
                                    note: note,
                                  ),
                                ),
                              );
                              setState(() {});
                            },
                            onLongPress: () async {
                              showDialog(
                                context: context,
                                builder: (context) {
                                  return AlertDialog(
                                    title: const Text(
                                        'Are you sure you want to delete this hiring?'),
                                    actions: [
                                      ElevatedButton(
                                        style: ButtonStyle(
                                          backgroundColor:
                                              MaterialStateProperty.all(
                                            Colors.red,
                                          ),
                                        ),
                                        onPressed: () async {
                                          await DatabaseHelper.deleteNote(note);
                                          Navigator.pop(context);
                                          setState(() {});
                                        },
                                        child: const Text('Yes'),
                                      ),
                                      ElevatedButton(
                                        onPressed: () => Navigator.pop(context),
                                        child: const Text('No'),
                                      ),
                                    ],
                                  );
                                },
                              );
                            },
                          ),
                          FutureBuilder<Widget>(
                            future:
                                DatabaseHelper().getApplyButton(note.id!),
                            builder: (context, snapshot) {
                              if (snapshot.connectionState ==
                                  ConnectionState.waiting) {
                                return const CircularProgressIndicator();
                              } else if (snapshot.hasError) {
                                return Text("Error: ${snapshot.error}");
                              } else {
                                return snapshot.data ?? Container();
                              }
                            },
                          )
                        ],
                      );
                    },
                    itemCount: snapshot.data!.length,
                  );
                }
                return const Center(
                  child: Text('No notes yet'),
                );
              }
              return const SizedBox.shrink();
            },
          ),
        ));
  }
}
