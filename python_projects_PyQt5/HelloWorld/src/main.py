import sys
from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow, QVBoxLayout, QWidget
from PyQt5.QtCore import Qt

class HelloWorldWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Hello, World!")
        self.setGeometry(100, 100, 400, 200)

        central_widget = QWidget()
        self.setCentralWidget(central_widget)

        layout = QVBoxLayout(central_widget)  # Passing the parent widget directly to the layout
        central_widget.setLayout(layout)

        label = QLabel("Hello, World!", self)
        label.setAlignment(Qt.AlignCenter) 
        layout.addWidget(label)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = HelloWorldWindow()
    window.show()
    sys.exit(app.exec_())
