import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QPushButton, QVBoxLayout, QWidget

class MyMainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        # Create a central widget to hold the QLabel and QPushButton
        central_widget = QWidget(self)
        self.setCentralWidget(central_widget)

        # Create a QVBoxLayout to arrange widgets vertically
        layout = QVBoxLayout(central_widget)

        # Create a QLabel with initial text
        self.label = QLabel("Initial Text", self)

        # Add the QLabel to the layout
        layout.addWidget(self.label)

        # Create a QPushButton
        self.button = QPushButton("Click me", self)
        # Connect the button's clicked signal to a custom slot (function)
        self.button.clicked.connect(self.on_button_click)

        # Add the QPushButton to the layout
        layout.addWidget(self.button)

        # Set the layout for the central widget
        central_widget.setLayout(layout)

        # Set window properties
        self.setWindowTitle("PyQt Button Click Example")
        self.setGeometry(100, 100, 400, 300)  # Set the window size

    def on_button_click(self):
        # Change the text of the QLabel when the button is clicked
        self.label.setText("Button Clicked")

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyMainWindow()
    window.show()
    sys.exit(app.exec_())
