import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QAction, QMenuBar, QMessageBox

class MyMainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.init_ui()

    def init_ui(self):
        # Create a menu bar
        menubar = self.menuBar()

        # Create the first menu with an action
        file_menu = menubar.addMenu('File')
        open_action = QAction('Open', self)
        open_action.triggered.connect(self.show_message_open)
        file_menu.addAction(open_action)

        # Create the second menu with an action
        edit_menu = menubar.addMenu('Edit')
        cut_action = QAction('Cut', self)
        cut_action.triggered.connect(self.show_message_cut)
        edit_menu.addAction(cut_action)

        # Set window properties
        self.setWindowTitle("PyQt Menu Example")
        self.setGeometry(100, 100, 400, 300)  # Set the window size

    def show_message_open(self):
        QMessageBox.information(self, 'Open Action', 'Open Action Triggered')

    def show_message_cut(self):
        QMessageBox.information(self, 'Cut Action', 'Cut Action Triggered')

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyMainWindow()
    window.show()
    sys.exit(app.exec_())
