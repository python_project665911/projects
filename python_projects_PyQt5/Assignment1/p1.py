import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QVBoxLayout, QWidget
from PyQt5.QtCore import Qt 

class MyMainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        # Create a central widget to hold the QLabel
        central_widget = QWidget(self)
        self.setCentralWidget(central_widget)

        # Create a QVBoxLayout to center the QLabel
        layout = QVBoxLayout(central_widget)

        # Create a QLabel with the text "Hello PyQt"
        label = QLabel("Hello PyQt", self)

        # Set alignment to center
        label.setAlignment(Qt.AlignCenter)

        # Add the QLabel to the layout
        layout.addWidget(label)

        # Set the layout for the central widget
        central_widget.setLayout(layout)

        # Set window properties
        self.setWindowTitle("PyQt Hello World")
        self.setGeometry(100, 100, 400, 300)  # Set the window size

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyMainWindow()
    window.show()
    sys.exit(app.exec_())
