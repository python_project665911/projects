import sys
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QLineEdit, QPushButton, QVBoxLayout, QHBoxLayout, QFileDialog, QRadioButton, QMessageBox
from PyQt5.QtGui import QPixmap


class UserProfileForm(QWidget):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("User Profile Form")
        self.setGeometry(100, 100, 400, 300)

        self.initUI()
        self.submitted_records = []  

    def initUI(self):
       
        self.first_name_label = QLabel("First Name:")
        self.first_name_edit = QLineEdit()
        self.last_name_label = QLabel("Last Name:")
        self.last_name_edit = QLineEdit()
        self.mobile_number_label = QLabel("Mobile Number:")
        self.mobile_number_edit = QLineEdit()
        self.height_label = QLabel("Height:")
        self.height_edit = QLineEdit()

       
        self.select_photo_button = QPushButton("Select Photo")
        self.select_photo_button.clicked.connect(self.selectPhoto)

        
        self.gender_label = QLabel("Gender:")
        self.male_radio = QRadioButton("Male")
        self.female_radio = QRadioButton("Female")

        
        self.submit_button = QPushButton("Submit")
        self.submit_button.clicked.connect(self.submitForm)

        self.record_label = QLabel()
        self.record_label.setWordWrap(True)

        form_layout = QVBoxLayout()
        form_layout.addWidget(self.first_name_label)
        form_layout.addWidget(self.first_name_edit)
        form_layout.addWidget(self.last_name_label)
        form_layout.addWidget(self.last_name_edit)
        form_layout.addWidget(self.mobile_number_label)
        form_layout.addWidget(self.mobile_number_edit)
        form_layout.addWidget(self.height_label)
        form_layout.addWidget(self.height_edit)
        form_layout.addWidget(self.select_photo_button)

        gender_layout = QHBoxLayout()
        gender_layout.addWidget(self.male_radio)
        gender_layout.addWidget(self.female_radio)
        form_layout.addWidget(self.gender_label)
        form_layout.addLayout(gender_layout)

        form_layout.addWidget(self.submit_button)
        form_layout.addWidget(self.record_label)

        self.setLayout(form_layout)

    def selectPhoto(self):
        options = QFileDialog.Options()
        file_name, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                   "All Files ();;Image Files (.png *.jpg *.jpeg)", options=options)
        if file_name:
            print(file_name) 
            self.selected_photo_path = file_name

    def submitForm(self):
        first_name = self.first_name_edit.text()
        last_name = self.last_name_edit.text()
        mobile_number = self.mobile_number_edit.text()
        height = self.height_edit.text()

        if not first_name or not last_name or not mobile_number or not height:
            QMessageBox.critical(self, "Error", "Please fill in all required fields.")
            return

        if not mobile_number.isdigit() or len(mobile_number) != 10:
            QMessageBox.critical(self, "Error", "Please enter a valid 10-digit mobile number.")
            return

        if not self.male_radio.isChecked() and not self.female_radio.isChecked():
            QMessageBox.critical(self, "Error", "Please select gender.")
            return

        record = {
            "First Name": first_name,
            "Last Name": last_name,
            "Mobile Number": mobile_number,
            "Height": height,
            "Gender": 'Male' if self.male_radio.isChecked() else 'Female',
            "Photo Path": self.selected_photo_path if hasattr(self, 'selected_photo_path') else None
        }
        self.submitted_records.append(record)

       
        QMessageBox.information(self, "Success", f"User Profile Submitted Successfully!\n"
                                                  f"First Name: {first_name}\n"
                                                  f"Last Name: {last_name}\n"
                                                  f"Mobile Number: {mobile_number}\n"
                                                  f"Height: {height}\n"
                                                  f"Gender: {'Male' if self.male_radio.isChecked() else 'Female'}")
        
        self.updateRecordLabel()


    def updateRecordLabel(self):
        records_text = "<h3>Submitted Records:</h3><br>"
        for record in self.submitted_records:
            records_text += f"<b>First Name:</b> {record['First Name']}<br>"
            records_text += f"<b>Last Name:</b> {record['Last Name']}<br>"
            records_text += f"<b>Mobile Number:</b> {record['Mobile Number']}<br>"
            records_text += f"<b>Height:</b> {record['Height']}<br>"
            records_text += f"<b>Gender:</b> {record['Gender']}<br>"
            if record['Photo Path']:
                records_text += f"<img src='{record['Photo Path']}' width='100'><br>"
            records_text += "<br>"
        self.record_label.setText(records_text)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    profile_form = UserProfileForm()
    profile_form.show()
    sys.exit(app.exec_())