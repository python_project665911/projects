import 'package:flutter/material.dart';
import '../ModelClass/model.dart';

class JobWidget extends StatelessWidget {
  final  job;
  final VoidCallback onTap;
  final VoidCallback onLongPress;
  const JobWidget(
      {Key? key,
        required this.job,
        required this.onTap,
        required this.onLongPress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onLongPress: onLongPress,
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 6),
        child: Card(
          elevation: 2,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    job.company,
                    style: const TextStyle(
                        fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                ),


                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 6, horizontal: 12),
                  child: Divider(
                    thickness: 1,
                  ),
                ),
                Text(job.jobId,
                    style: const TextStyle(
                        fontSize: 12, fontWeight: FontWeight.w300,color: Colors.grey),
                    ),


                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 6, horizontal: 12),
                  child: Divider(
                    thickness: 1,
                  ),
                ),
                Text(job.jobRole,
                    style: const TextStyle(
                        fontSize: 16, fontWeight: FontWeight.w400
                        )
                    ),


                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 6, horizontal: 12),
                  child: Divider(
                    thickness: 1,
                  ),
                ),
                Text(job.jd,
                    style: const TextStyle(
                        fontSize: 16, fontWeight: FontWeight.w400))

              ],
            ),
          ),
        ),
      ),
    );
  }
}