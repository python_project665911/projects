//model class for recruiter

class Job{
  final String company;
  final String jobRole;
  final String jobId;
  final String jd;
  final int? id;

  const Job(
    {
      this.id,
      required this.company, 
      required this.jobRole, 
      required this.jobId, 
      required this.jd
    }
  );

  factory Job.fromJson(Map<String,dynamic> json) => Job(

    id: json['id'],
    company: json['company'],
    jobRole: json['jobRole'],
    jobId : json['jobId'],
    jd : json['jd'],
    
  );

  Map<String,dynamic> toJson() => {
    'id': id,
    'company': company,
    'jobRole': jobRole,
    'jobId':jobId,
    'jd':jd
  };
}